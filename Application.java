public class Application
{
	public static void main(String[] args)
	{
		Cat tux = new Cat();
		Cat mittens = new Cat();
		
		tux.age = 2;
		System.out.println("Age: "+tux.age);
		tux.name = "Tux";
		System.out.println("Name: "+tux.name);
		tux.colours = "Black and White";
		System.out.println("Colours: "+tux.colours);
		tux.attitude = "Energetic";
		System.out.println("Attitude: "+tux.attitude);
		
		mittens.age = 5;
		System.out.println("Age: "+mittens.age);
		mittens.name = "Mittens";
		System.out.println("Name: "+mittens.name);
		mittens.colours = "White";
		System.out.println("Colours: "+mittens.colours);
		mittens.attitude = "Calm";
		System.out.println("Attitude: "+mittens.attitude);
		
		System.out.println(" ");
		tux.sayHi();
		mittens.sayHi();
		
		Cat[] clowder = new Cat[3];
		clowder[0] = tux;
		clowder[1] = mittens;
		System.out.println(clowder[0].name);
		
		System.out.println(" ");
		
		clowder[2] = new Cat();
		clowder[2].age = 3;
		System.out.println("Age: "+clowder[2].age);
		clowder[2].name = "paws";
		System.out.println("Name: "+clowder[2].name);
		clowder[2].colours = "Orange and Brown";
		System.out.println("Colours: "+clowder[2].colours);
		clowder[2].attitude = "Curious";
		System.out.println("Attitude: "+clowder[2].attitude);
	}
}