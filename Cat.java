public class Cat
{
	public int age;
	public String name;
	public String colours;
	public String attitude;
	
	public void sayHi()
	{
		System.out.println("Hi "+this.name);
	}
	
	public void attitudeChecker()
	{
		if (this.attitude.equals("Calm"))
		{
			System.out.println("My Cat Windy Is Calm Too!");
		}
		else if (this.attitude.equals("Energetic"))
		{
			System.out.println("My Cat Thunder Is Energetic Too!");
		}
		else
		{
			System.out.println("My Cats are not "+this.attitude);
		}
	}
}